`smart boot`是以**spring boot**为原型而构建的一款技术架构模型，致力于为企业的微服务方案实施提供基础服务。`smart boot`的本质就是一个spring boot工程，因此对于熟悉spring boot的开发者而言，掌握smart boot的几乎没什么门槛。即便仅是了解spring，也能在短时间内上手。


##工程结构

- smart-assembly   
用于集中管理smart-boot工程中各Spring配置文件所需的属性配置
- smart-dal  
数据操作层bundle,实现数据存储读取操作。
- smart-service-integration  
与外部第三方系统对接的bundle，以供smart-boot调用第三方服务
- smart-component  
组件bundle，遵循单一职责原则，向下对接smart-dal、smart-service-integration，向上为业务层smart-service-impl提供各组件式服务
- smart-service-facade  
定义smart-boot的服务接口，一个独立的bundle,不依赖其他模块。未来第三方系统可通过该bundle提供的接口调用服务
- smart-service-impl  
该bundle通过引用smart-componet提供的各组件用于实现smart-service-facade中定义的接口。对于私有服务可直接在本bundle中定义接口，无需放置在smart-service-facade中.  

>为方便使用，也可直接调用smart-service-integration中提供的服务

- smart-shared  
该bundle完全独立于业务，主要用于提供一下工具类，可被任一bundle引用
- smart-restful  
Web层，仅负责前后端的数据交互，不建议在该bundle中进行复杂的业务处理，应统一交由smart-service-impl处理

![系统结构图](images/1.png)

##smart-boot特点
- 研发：
	模块化编程、面向服务编程、测试框架
- 部署：
	eclipse，tomcat
- 管控：
	统一上下文，动态路由，监控日志，地址池，故障隔离，精细化管控，jvm监控。。。

##smart-boot依赖
1. Mysql 默认对接mysql数据库，可根据实际项目需要重新进行配置
2. Redis 默认使用redis提供缓存服务，可根据实际项目需要重新进行配置
3. [smart-sosa](https://git.oschina.net/smartdms/smart-sosa) 提供底层RPC服务
4. [maven-mybatisdalgen-plugin](https://git.oschina.net/smartdms/maven-mybatisdalgen-plugin) 数据层采用了mybatis框架，通过该maven-mybatisdalgen-plugin插件可以方便的生成DAL层的代码以及配置文件。

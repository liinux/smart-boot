##创建smart-boot工程
如果您想将smartboot用于自己的项目中,直接下载smartboot源码并不可取，因为还得将包名以及pom.xml文件中的groupId、artifactId根据您的实际情况做修改。
为了提供更好的使用体验，我们特地制作了**smartboot-archetype**（如果您不知道什么是archetype,推荐先去[Maven](http://maven.apache.org/guides/introduction/introduction-to-archetypes.html)官网了解一下）

言归正传，构建一个smartboot工程只需执行一个maven命令即可。

	mvn -X archetype:generate -DarchetypeGroupId=org.smartboot.maven.archetype -DarchetypeArtifactId=smartboot-archetype -DarchetypeVersion=1.0.0 -DarchetypeCatalog=remote -DgroupId=com.test -DartifactId=hello-world -Dversion=1.0.0 -Dpackage=com.test

参数说明:

- -DarchetypeGroupId=org.smartboot.maven.archetype -DarchetypeArtifactId=smartboot-archetype -DarchetypeVersion=1.0.0 -DarchetypeCatalog=remote	
用于指定脚手架的版本,参照当前示例即可
	
- -DgroupId=com.test -DartifactId=hello-world -Dversion=1.0.0 -Dpackage=com.test	
根据您的项目指定groupId、artifactId、version、package

